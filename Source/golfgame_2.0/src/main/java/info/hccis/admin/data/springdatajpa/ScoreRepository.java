package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.GamePlayer;
import info.hccis.admin.model.jpa.Score;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScoreRepository extends CrudRepository<Score, Integer> {
    List<Score> findByGameIdAndUserId(int gameId, int userId);
    List<Score> findByGameIdAndUserIdAndHoleNumber(int gameId, int userId, int holeNumber);
}