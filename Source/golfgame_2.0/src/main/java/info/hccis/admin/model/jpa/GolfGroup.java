/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author BJMacLean
 */
@Entity
@Table(name = "GolfGroup")
@NamedQueries({
    @NamedQuery(name = "GolfGroup.findAll", query = "SELECT g FROM GolfGroup g")})
public class GolfGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "groupId")
    private Integer groupId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gameId")
    private int gameId;
    @Column(name = "userId1")
    private Integer userId1;
    @Column(name = "userId2")
    private Integer userId2;
    @Column(name = "userId3")
    private Integer userId3;
    @Column(name = "userId4")
    private Integer userId4;

    public GolfGroup() {
    }

    public GolfGroup(Integer groupId) {
        this.groupId = groupId;
    }

    public GolfGroup(Integer groupId, int gameId) {
        this.groupId = groupId;
        this.gameId = gameId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public Integer getUserId1() {
        return userId1;
    }

    public void setUserId1(Integer userId1) {
        this.userId1 = userId1;
    }

    public Integer getUserId2() {
        return userId2;
    }

    public void setUserId2(Integer userId2) {
        this.userId2 = userId2;
    }

    public Integer getUserId3() {
        return userId3;
    }

    public void setUserId3(Integer userId3) {
        this.userId3 = userId3;
    }

    public Integer getUserId4() {
        return userId4;
    }

    public void setUserId4(Integer userId4) {
        this.userId4 = userId4;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupId != null ? groupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GolfGroup)) {
            return false;
        }
        GolfGroup other = (GolfGroup) object;
        if ((this.groupId == null && other.groupId != null) || (this.groupId != null && !this.groupId.equals(other.groupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.GolfGroup[ groupId=" + groupId + " ] group players="+userId1+","+userId2+","+userId3+","+userId4+"";
    }
    
}
