/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author BJMacLean
 */
@Entity
@Table(name = "GamePlayer")
@NamedQueries({
    @NamedQuery(name = "GamePlayer.findAll", query = "SELECT g FROM GamePlayer g")})
public class GamePlayer implements Serializable {

    @Transient
    private User user;

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    
    
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gamePlayerId")
    private Integer gamePlayerId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gameId")
    private int gameId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userId")
    private int userId;
    @Column(name = "score")
    private Integer score;
    @Column(name = "points")
    private Integer points;

    public GamePlayer() {
    }

    public GamePlayer(Integer gamePlayerId) {
        this.gamePlayerId = gamePlayerId;
    }

    public GamePlayer(Integer gamePlayerId, int gameId, int userId) {
        this.gamePlayerId = gamePlayerId;
        this.gameId = gameId;
        this.userId = userId;
    }

    public Integer getGamePlayerId() {
        return gamePlayerId;
    }

    public void setGamePlayerId(Integer gamePlayerId) {
        this.gamePlayerId = gamePlayerId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gamePlayerId != null ? gamePlayerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GamePlayer)) {
            return false;
        }
        GamePlayer other = (GamePlayer) object;
        if ((this.gamePlayerId == null && other.gamePlayerId != null) || (this.gamePlayerId != null && !this.gamePlayerId.equals(other.gamePlayerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.GamePlayer[ gamePlayerId=" + gamePlayerId + " ]";
    }
    
}
