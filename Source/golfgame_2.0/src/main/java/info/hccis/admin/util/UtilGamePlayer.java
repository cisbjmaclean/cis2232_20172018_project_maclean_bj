package info.hccis.admin.util;

import info.hccis.admin.data.springdatajpa.GamePlayerRepository;
import info.hccis.admin.model.jpa.GamePlayer;
import java.util.ArrayList;

/**
 * This class will hold generic business functionality for game players.
 * @author BJMacLean
 * @since 20170513
 */
public class UtilGamePlayer {
    
    /**
     * This method will get the game player from db if one exists otherwise if will create a new one.
     * @since 20170513
     * @author BJM
     * 
     * @return the game player
     */
    
    public static GamePlayer getGamePlayerForUserIdAndGameId(GamePlayerRepository gpr, int userId, Integer gameId){
        ArrayList<GamePlayer> gps =  (ArrayList<GamePlayer>) gpr.findByGameIdAndUserId(gameId, userId);
        GamePlayer gp;
        if (gps == null || gps.size() ==0) {
            System.out.println("Did not Find game player");
            gp = new GamePlayer(0, gameId, userId);
            //Save the game player so the gp id will be set.
            gp = gpr.save(gp);
        }else{
            System.out.println("Found game player");
            gp = gps.get(0);
        }
        return gp;
    }
    
    
    
}
