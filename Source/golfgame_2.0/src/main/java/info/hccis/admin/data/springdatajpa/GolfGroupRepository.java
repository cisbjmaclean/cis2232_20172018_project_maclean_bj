package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.GolfGroup;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GolfGroupRepository extends CrudRepository<GolfGroup, Integer> {

    List<GolfGroup> findByGameIdAndUserId1(int gameId, int userId);

}
