package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.GameRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.Game;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.service.CodeService;
import info.hccis.admin.util.UtilCodes;
import info.hccis.admin.util.UtilGames;
import info.hccis.admin.util.UtilSecurity;
import info.hccis.admin.util.Utility;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;
    private final GameRepository gr;
    private final CodeService codeService;

    @Autowired
    public OtherController(CodeService codeService, CodeTypeRepository ctr, CodeValueRepository cvr, GameRepository gr) {
        this.codeService = codeService;
        this.ctr = ctr;
        this.cvr = cvr;
        this.gr = gr;
    }

    @RequestMapping("/")
    public String showHome(Model model, HttpServletRequest request) {
        
        HttpSession session = request.getSession();
        session.removeAttribute("loggedIn");
        session.setAttribute("message", "");
        
        System.out.println("in controller for /");
        //setup the databaseConnection object in the model.  This will be used on the 
        //view.
        System.out.println("MD5 hash for 123=" + Utility.getHashPassword("123"));
        DatabaseConnection databaseConnection = new DatabaseConnection();
        model.addAttribute("databaseConnection", databaseConnection);
        UtilCodes.updateSessionCodes(request, codeService);

        ArrayList<Game> games = (ArrayList<Game>) gr.findAll();

        UtilGames.setCourseNamesForGames(request, games);

        session.setAttribute("games", games);

        //**********************************************************************
        //Load users into the session for drop down.
        //**********************************************************************
        
        ArrayList<User> users = codeService.getUsers();
        session.setAttribute("users", users);
        
        //**********************************************************************
        //Put the user in the session/model for future use.
        //**********************************************************************
        
        User user = new User();
        session.setAttribute("loggedIn", user);
        model.addAttribute("user", user);

        //numbers used for scoring
        ArrayList<Integer> scores = new ArrayList();
        for(int i=0; i<=12; i++){
            scores.add(i);
        }
        request.getSession().setAttribute("scores", scores);
        
        //Put the hole numbers in the session for selection by users.
        ArrayList<String> doubleHoles = new ArrayList();
        for(int i=1; i<19; i++){
            doubleHoles.add(""+i);
            //doubleHoles.add(""+i+"B");  what about double press??  Guess add forboth teams
        }
        request.getSession().setAttribute("holeNumbers",doubleHoles);

        
        return "/welcome";
    }

    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }


    @RequestMapping("/other/about")
    public String showAbout(Model model) {
        // System.out.println("Count from code_type="+ctr.count());

//        ArrayList<CodeType> cts = (ArrayList<CodeType>) ctr.findByEnglishDescription("User Types");
//        System.out.println("BJM-"+cts);
//        
//        ArrayList<CodeType> codeTypesByBob = (ArrayList<CodeType>) ctr.findByCreatedUserId("Bob");
//     
//        ArrayList<CodeValue> codeValues = (ArrayList<CodeValue>) cvr.findAll();
//        System.out.println("Here are the Code values");     
//        for(CodeValue thisOne: codeValues){
//            System.out.println(thisOne);
//        }
//        System.out.println("Here are the Code types created by bob");     
//        for(CodeType thisOne: codeTypesByBob){
//            System.out.println(thisOne);
//        }
//        
//        //Create a new code value here.
//        CodeValue newCodeValue = new CodeValue(4,1,"TestNew","TN2");
//        cvr.save(newCodeValue);
        return "other/about";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }

}
