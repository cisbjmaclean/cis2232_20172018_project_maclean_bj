package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.GameLBLT;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameLBLTRepository extends CrudRepository<GameLBLT, Integer> {


}