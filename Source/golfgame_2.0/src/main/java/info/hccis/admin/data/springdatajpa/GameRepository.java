package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.Game;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends CrudRepository<Game, Integer> {


}