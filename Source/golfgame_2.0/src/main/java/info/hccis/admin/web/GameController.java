package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.GameRepository;
import info.hccis.admin.data.springdatajpa.GolfGroupRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.jpa.Game;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.util.UtilGames;
import info.hccis.admin.util.UtilSecurity;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GameController {

    private final GameRepository gr;
    private final UserRepository ur;
    private final GolfGroupRepository ggr;

    @Autowired
    public GameController(GameRepository gr, UserRepository ur, GolfGroupRepository ggr) {
        this.gr = gr;
        this.ur = ur;
        this.ggr = ggr;
    }

    @RequestMapping("/game/list")
    public String gameList(Model model, HttpServletRequest request, @ModelAttribute("user") User user) {

        //Validate that the user is logged in.
        String buttonName = request.getParameter("action");
        if (buttonName == null || !buttonName.equals("start")) {
            if (!UtilSecurity.validUser(request.getSession())) {
                request.getSession().setAttribute("message", "Please login");
                return "/welcome";
            }
        }

        if (!(user == null) && user.getUserId() != null && user.getUserId() > 0) {
            user = ur.findOne(user.getUserId());
            System.out.println(user);
            request.getSession().setAttribute("loggedIn", user);
        }

        System.out.println("In gameList");
        ArrayList<Game> games = (ArrayList<Game>) gr.findAll();

        UtilGames.setCourseNamesForGames(request, games);

        request.getSession().setAttribute("games", games);
        return "/game/list";
    }

    @RequestMapping("/game/add")
    public String gameAdd(Model model) {
        Game game = new Game();
        model.addAttribute("game", game);
        return "game/add";
    }

    @RequestMapping("/game/addSubmit")
    public String gameAddSubmit(Model model, HttpServletRequest request, @Valid @ModelAttribute("game") Game game, BindingResult result) {

        System.out.println("In gameAddSubmit gameId=" + game.getGameId());
        game.setGameId(0);
        gr.save(game);
        ArrayList<Game> games = (ArrayList<Game>) gr.findAll();

        UtilGames.setCourseNamesForGames(request, games);
        request.getSession().setAttribute("games", games);
        return "/game/list";
    }

    @RequestMapping("/game/delete")
    public String gameDelete(Model model, HttpServletRequest request) {

        System.out.println("In gameDelete gameId=" + request.getParameter("id"));
        gr.delete(Integer.parseInt(request.getParameter("id")));
        ArrayList<Game> games = (ArrayList<Game>) gr.findAll();
        UtilGames.setCourseNamesForGames(request, games);
        request.getSession().setAttribute("games", games);
        return "/game/list";
    }
//    @RequestMapping("/game/score")
//    public String gameScore(Model model, HttpServletRequest request) {
// 
//        String gameIdString = request.getParameter("id");
//        System.out.println("In gameScore gameId="+gameIdString);
//        Game game = new Game(Integer.parseInt(gameIdString));
//        model.addAttribute("game",game);
//        return "/game/score";
//    }
}
