package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.GameLBLTRepository;
import info.hccis.admin.data.springdatajpa.GamePlayerRepository;
import info.hccis.admin.data.springdatajpa.GameRepository;
import info.hccis.admin.data.springdatajpa.GolfGroupRepository;
import info.hccis.admin.data.springdatajpa.ScoreRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.entity.Group;
import info.hccis.admin.model.entity.ScoreCard;
import info.hccis.admin.model.jpa.GameLBLT;
import info.hccis.admin.model.jpa.GamePlayer;
import info.hccis.admin.model.jpa.GolfGroup;
import info.hccis.admin.model.jpa.Score;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.util.UtilGamePlayer;
import info.hccis.admin.util.UtilGames;
import info.hccis.admin.util.Utility;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GroupController {

    private final ScoreRepository sr;
    private final GamePlayerRepository gpr;
    private final UserRepository ur;
    private final GameRepository gr;
    private final GolfGroupRepository ggr;
    private final GameLBLTRepository glbltr;

    @Autowired
    public GroupController(ScoreRepository sr, GamePlayerRepository gpr, UserRepository ur, GameRepository gr, GolfGroupRepository ggr, GameLBLTRepository glbltr) {
        this.sr = sr;
        this.gpr = gpr;
        this.ur = ur;
        this.gr = gr;
        this.ggr = ggr;
        this.glbltr = glbltr;
    }

    @RequestMapping("/group/add")
    public String groupAdd(Model model, HttpServletRequest request) {

        //Get the logged in userId
        User loggedIn = (User) request.getSession().getAttribute("loggedIn");

        String gameIdString = request.getParameter("id");
        int gameId = Integer.parseInt(gameIdString);
        ScoreCard scorecard = new ScoreCard(Integer.parseInt(gameIdString));

        request.getSession().setAttribute("scorecard", scorecard);

        if(loggedIn == null || loggedIn.getUserId()==null || loggedIn.getUserId()==0){
            model.addAttribute("message","Must be logged in to create a group.");
            return "/other/notice";
        }
        
        Group group = new Group();
        ArrayList<GolfGroup> gg = (ArrayList<GolfGroup>) ggr.findByGameIdAndUserId1(gameId, loggedIn.getUserId());
        String returnLocation = "/group/add";

        //***********************************************************************************
        // If find a golf group for the logged in user, just go directly to that group.
        //
        //***********************************************************************************
        if (gg.size() > 0) {
            GolfGroup ggTemp = gg.get(0);  //should only be one group for the logged in user and currentPlayerId game
            group.setGameId(gameId);
            ArrayList<Integer> tempPlayerIds = new ArrayList();

            tempPlayerIds.add(ggTemp.getUserId1());
            tempPlayerIds.add(ggTemp.getUserId2());
            tempPlayerIds.add(ggTemp.getUserId3());
            tempPlayerIds.add(ggTemp.getUserId4());

            group.setPlayerIds(tempPlayerIds);
            group.setUsers(loadUsers(ggTemp));

            //******************************************************************
            // also load the lblt game details from the db.  Need to get presses
            // and greeny details.
            //******************************************************************
            GameLBLT gameLBLT = glbltr.findOne(gameId);
            if (gameLBLT != null) {
                String[] presses = gameLBLT.getTeam1presses().split(",");
                int counter = 0;
                for (String nextPress : presses) {
                    if (nextPress != null && !nextPress.isEmpty() && !nextPress.equals("0")) {
                        group.getPressHolesTeam1()[counter] = nextPress;
                        group.getPressHoles()[counter] = Integer.parseInt(nextPress); //Also load the overall presses for ease of scoring.
//                        group.getPresses()[counter] = Integer.parseInt(nextPress);
//                        if (Integer.parseInt(nextPress) > 0) {
//                            group.getPressDescriptions()[counter] = "Team 1 pressed on " + nextPress;
//                        }
                        counter++;
                    }
                }
                presses = gameLBLT.getTeam2presses().split(",");
                int team1Presses = counter;
                counter = 0;
                for (String nextPress : presses) {
                    if (nextPress != null && !nextPress.isEmpty()) {
                        group.getPressHolesTeam2()[counter] = nextPress;
                        group.getPressHoles()[counter + team1Presses] = Integer.parseInt(nextPress); //Also load the overall presses for ease of scoring.
//                        group.getPresses()[counter] = Integer.parseInt(nextPress);
//                        if (Integer.parseInt(nextPress) > 0) {
//                            group.getPressDescriptions()[counter] = "Team 2 pressed on " + nextPress;
//                        }
                        counter++;
                    }
                }
                Arrays.sort(group.getPressHoles());
                System.out.println("Before sort:" + Arrays.toString(group.getPressHoles()));
                //Now reverse
                int[] copyPresses = Arrays.copyOf(group.getPressHoles(), group.getPressHoles().length);
                for (int i = 0; i < copyPresses.length; i++) {
                    group.getPressHoles()[i] = copyPresses[copyPresses.length - 1 - i];
                }
                System.out.println("After sort:" + Arrays.toString(group.getPressHoles()));

                //GREENIES
                String[] greenies = gameLBLT.getTeam1GreenyPoints().split(",");
                counter = 0;
                for (String nextOne : greenies) {
                    if (nextOne != null && !nextOne.isEmpty() && !nextOne.equals("0")) {
                        group.getGreenyHolesTeam1()[counter] = nextOne;
                        counter++;
                    }
                }
                greenies = gameLBLT.getTeam2GreenyPoints().split(",");

                counter = 0;
                for (String nextOne : greenies) {
                    if (nextOne != null && !nextOne.isEmpty() && !nextOne.equals("0")) {
                        group.getGreenyHolesTeam2()[counter] = nextOne;
                        counter++;
                    }
                }

                //BIRDIES
                String[] birdies;
                try {
                    birdies = gameLBLT.getTeam1BirdyPoints().split(",");
                    counter = 0;
                    for (String nextOne : birdies) {
                        if (nextOne != null && !nextOne.isEmpty() && !nextOne.equals("0")) {
                            group.getBirdyHolesTeam1()[counter] = nextOne;
                            counter++;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("No team 1 birdies");
                }

                try {
                    birdies = gameLBLT.getTeam2BirdyPoints().split(",");
                    counter = 0;
                    for (String nextOne : birdies) {
                        if (nextOne != null && !nextOne.isEmpty() && !nextOne.equals("0")) {
                            group.getBirdyHolesTeam2()[counter] = nextOne;
                            counter++;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("No team 2 birdies");
                }
                //Get the greeny info
//                String greeny1 = gameLBLT.getTeam1GreenyPoints();
//                String[] greeny1Parts = greeny1.split(",");
//                int counterG = 0;
//                int greenyTotal1 = 0;
//                String g1Greenies = group.getGreeny1Details();
//                for (String current : greeny1Parts) {
//                    if (current != null && !current.isEmpty()) {
//                        if (Integer.parseInt(current) > 0) {
//                            group.getGreenyPointsTeam1()[counterG] = Integer.parseInt(current);
//                            greenyTotal1 += getLBLTPointsForHole(Integer.parseInt(current), group.getPresses());
//                            g1Greenies += " " + current;
//                            group.setGreeny1Details(g1Greenies);
//                        }
//                    }
//                    counterG++;
//                }
//                group.setGreeny1(greenyTotal1);
//
//                String greeny2 = gameLBLT.getTeam2GreenyPoints();
//                String[] greeny2Parts = greeny2.split(",");
//                int counterG2 = 0;
//                int greenyTotal2 = 0;
//                String g2Greenies = group.getGreeny2Details();
//                for (String current : greeny2Parts) {
//                    if (current != null && !current.isEmpty()) {
//                        if (Integer.parseInt(current) > 0) {
//                            group.getGreenyPointsTeam2()[counterG2] = Integer.parseInt(current);
//                            greenyTotal2 += getLBLTPointsForHole(Integer.parseInt(current), group.getPresses());
//                            g2Greenies += " " + current;
//                            group.setGreeny2Details(g2Greenies);
//                        }
//                    }
//                    counterG2++;
//                }
//                group.setGreeny2(greenyTotal2);
//                

            }
            group.setLoaded(1);
            returnLocation = "/group/scoreLBLT";

        } //**************************************************************************************
        // If there was not a group the setup to send them to pick their group.  
        // 
        //**************************************************************************************
        else {
            group.setGameId(gameId);
            group.getPlayerIds().set(0, loggedIn.getUserId()); //set the logged in user to the first group member.
        }

        //******************************************************************
        //Load the group score details
        //*******************************************************************
        loadGroupPlayerScoreDetails(group);

        //Not sure if this is necessary.  Will remove it 
//        //***********************************************************************
//        //Load the player info
//        //***********************************************************************
//        ArrayList<Integer> gamePlayerIds = new ArrayList();
//        for (Integer currentPlayerId : group.getPlayerIds()) {
//            System.out.println(currentPlayerId);
//            gamePlayerIds.add(UtilGamePlayer.getGamePlayerForUserIdAndGameId(gpr, currentPlayerId, group.getGameId()).getGamePlayerId());
//        }
//        //**********************************************************************
//        //Now add them to the group.
//        group.setGamePlayerIds(gamePlayerIds);
        request.getSession().setAttribute("lastSave", group);
        model.addAttribute("group", group);

        return returnLocation;
    }

    /**
     * Load the player information to be displayed on the score page.
     *
     * @since 20170516
     * @author BJM
     *
     */
    public void loadGroupPlayerScoreDetails(Group group) {

        int gameId = group.getGameId();

        //***************************************************************************
        //Also want to set any existing scores which may exist for the players that
        //are being added to this group.  For new group this would only get the 
        //logged in player info (if it exists).
        //***************************************************************************
        int counter = 0; //Used to know which player index.
        for (int currentPlayerId : group.getPlayerIds()) {

            //*********************************************************************
            //Get the GamePlayer info
            //*********************************************************************
            GamePlayer gp = null;
            boolean foundGamePlayer = false;
            try {
                if (Utility.debugging) {
                    System.out.println("Trying to get game player info for gameId=" + gameId + " and playerId=" + currentPlayerId);
                }
                gp = gpr.findByGameIdAndUserId(gameId, currentPlayerId).get(0);
                foundGamePlayer = true;
            } catch (IndexOutOfBoundsException iobe) {
                System.out.println("count not find game player for gameId=" + gameId + " and playerId=" + currentPlayerId);
            }

            //***************************************************************************
            //Only load player info if one was found in db.
            //***************************************************************************
            if (foundGamePlayer && gp.getPoints() != null) { //bjm todo - why would points be null... should look into this.
                group.getChicagoPoints()[counter] = gp.getPoints();

                //*********************************************************************
                //Get the score info
                //*********************************************************************
                ArrayList<Score> scores = (ArrayList<Score>) sr.findByGameIdAndUserId(gameId, currentPlayerId);
                Collections.sort(scores); //scores will sort by hole number.
                System.out.println("Found " + scores.size() + " scores for gameId=" + gameId + " and playerId=" + currentPlayerId);

                int front = 0;
                int back = 0;
                //***********************************************************************
                //Load the scores into the group
                //***********************************************************************
                for (Score score : scores) {
                    System.out.println("Setting scores for hole number index=" + (score.getHoleNumber() - 1));
                    if (score.getHoleNumber() <= 9) {
                        group.getPlayerScoresFront()[counter][score.getHoleNumber() - 1] = score.getShotsTaken();
                        front += score.getShotsTaken();
                    } else {
                        group.getPlayerScoresBack()[counter][score.getHoleNumber() - 10] = score.getShotsTaken();
                        back += score.getShotsTaken();
                    }
                }
                group.getPlayerScoreFront()[counter] = front;
                group.getPlayerScoreBack()[counter] = back;
            }
            counter++; //Move on to the next player.
        }
    }

    /**
     * Load the users based on the user ids which are in the golf group.
     *
     * @since 20170514
     * @author BJM
     * @param ggTemp
     * @return list of users
     */
    public ArrayList<User> loadUsers(GolfGroup ggTemp) {
        ArrayList<User> tempUsers = new ArrayList();
        User user1 = null;
        User user2 = null;
        User user3 = null;
        User user4 = null;

        try {
            user1 = ur.findOne(ggTemp.getUserId1());
            user2 = ur.findOne(ggTemp.getUserId2());
            user3 = ur.findOne(ggTemp.getUserId3());
            user4 = ur.findOne(ggTemp.getUserId4());
        } catch (Exception e) {
            System.out.println("Error getting one of the users for the group");
        }
        tempUsers.add(user1);
        tempUsers.add(user2);
        tempUsers.add(user3);
        tempUsers.add(user4);

        return tempUsers;
    }

    /**
     * This controller method will be used when the user is setting up their
     * group. They will have come from the page where they selected the group
     * players (4 of them).
     *
     * @since 20170516
     * @author BJM
     *
     * @param model
     * @param request
     * @param group
     * @return
     */
    @RequestMapping("/group/addSubmit")
    public String groupAddSubmit(Model model, HttpServletRequest request, @ModelAttribute("group") Group group) {

        System.out.println("In groupAddSubmit gameId=" + group.getGameId());
        GolfGroup gg = new GolfGroup();

        gg.setGameId(group.getGameId());
        gg.setUserId1(group.getPlayerIds().get(0));
        gg.setUserId2(group.getPlayerIds().get(1));
        gg.setUserId3(group.getPlayerIds().get(2));
        gg.setUserId4(group.getPlayerIds().get(3));

        if (Utility.debugging) {
            System.out.println("adding group for game: " + gg);
        }

        //***********************************************************************************
        //Also want to register the player as a game player.  Players can be in groups
        //but they should also be game players for the overall scores to be registered for
        //this game.        
        //***********************************************************************************
        ggr.save(gg);

        ArrayList<Integer> gamePlayerIds = new ArrayList();
        for (Integer currentPlayerId : group.getPlayerIds()) {
            System.out.println(currentPlayerId);
            //******************************************************************
            //Get the game player id based on the currentPlayerId and gameId
            //******************************************************************
            gamePlayerIds.add(UtilGamePlayer.getGamePlayerForUserIdAndGameId(gpr, currentPlayerId, group.getGameId()).getGamePlayerId());
        }
        //*******************************************************************
        //Now add them to the group.
        //*******************************************************************
        group.setGamePlayerIds(gamePlayerIds);

        //*******************************************************************
        //Load the users based on the golf group user ids.
        //*******************************************************************
        group.setUsers(loadUsers(gg));

        //Load the player score details
        loadGroupPlayerScoreDetails(group);

        //**********************************************************************
        // Also want to load the
        //*******************************************************************
        //Also store the group to the session.  
        //*******************************************************************
        request.getSession().setAttribute("lastSave", group);

        model.addAttribute("group", group);

        return "/group/scoreLBLT";
    }

    /**
     * This method will be used when the user chooses to save the scores for a
     * group.
     *
     * @since 20170516
     * @author BJM
     *
     * @param model
     * @param request
     * @param group
     * @return
     */
    @RequestMapping("/group/addGroupScoreSubmit")
    public String addGroupScoreSubmit(Model model, HttpServletRequest request, @ModelAttribute("group") Group group) {

        //BJM todo have to refactor the way these are handled.
        //test getting multiple
        System.out.println("pressHolesTeam1=" + Arrays.toString(group.getPressHolesTeam1()));

        //***************************************************************************
        //This will be the full group information.  Some of the attributes are not
        //hidden on the form so may not be available from the group coming in 
        //as the model attribute.
        //***************************************************************************
        Group lastSave = (Group) request.getSession().getAttribute("lastSave");
        lastSave.setLoaded(1);
        group.setLoaded(0);

        int[][] scores = new int[4][18];

        //******************************************************************
        //Load an 18 hole array
        //******************************************************************
        int nextHoleNumber = 1;
        for (int playerNum = 0; playerNum < 4; playerNum++) {
            for (int i = 0; i < 9; i++) {
                scores[playerNum][i] = group.getPlayerScoresFront()[playerNum][i];
                scores[playerNum][i + 9] = group.getPlayerScoresBack()[playerNum][i];
            }
        }

        //******************************************************************
        //Find out what hole is next.  Only have to calc up to this point
        //******************************************************************
        boolean foundNextHole = false;
        for (int i = 0; i < 18; i++) {
            if (scores[0][i] == 0) {
                foundNextHole = true;
                nextHoleNumber = i + 1; //next hole determined by player 1 not having a score entered.
                System.out.println("The next hole is #" + i);
                break;
            }
        }
        if (!foundNextHole) {
            nextHoleNumber = 19;
        }

        //************************************************************************
        // Check which button was clicked
        //************************************************************************
        String buttonName = request.getParameter("action");
        System.out.println("Button clicked = " + buttonName);
        boolean configChanged = false;
        if (buttonName != null && buttonName.equalsIgnoreCase("saveDetails")) {
            configChanged = true;
        }

        //************************************************************************
        //reload the presses from the form.
        //************************************************************************
        int currentNumberOfPresses = 0;
        for (String i : group.getPressHolesTeam1()) {
            if (i.compareTo("0") > 0) {
                group.getPressHoles()[currentNumberOfPresses] = Integer.parseInt(i);
                currentNumberOfPresses += 1;
            }
        }
        for (String i : group.getPressHolesTeam2()) {
            if (i.compareTo("0") > 0) {
                group.getPressHoles()[currentNumberOfPresses] = Integer.parseInt(i);
                currentNumberOfPresses += 1;
            }
        }

        group.setNumberOfPresses(currentNumberOfPresses);

        Arrays.sort(group.getPressHoles());
        System.out.println("Before sort:" + Arrays.toString(group.getPressHoles()));
        //Now reverse
        int[] copyPresses = Arrays.copyOf(group.getPressHoles(), group.getPressHoles().length);
        for (int i = 0; i < copyPresses.length; i++) {
            group.getPressHoles()[i] = copyPresses[copyPresses.length - 1 - i];
        }
        System.out.println("After sort:" + Arrays.toString(group.getPressHoles()));

//These come from the form!!
//        group.setPressHoles(lastSave.getPressHoles());
//        group.setPressHolesTeam1(lastSave.getPressHolesTeam1());
//        group.setPressHolesTeam2(lastSave.getPressHolesTeam2());
//
//        group.setGreenyHolesTeam1(lastSave.getGreenyHolesTeam1());
//        group.setGreenyHolesTeam2(lastSave.getGreenyHolesTeam2());
//
//        group.setBirdyHolesTeam1(lastSave.getBirdyHolesTeam1());
//        group.setBirdyHolesTeam2(lastSave.getBirdyHolesTeam2());
//        group.setPresses(lastSave.getPresses());
//        group.setPressDescriptions(lastSave.getPressDescriptions());
//        group.setGreeny1(lastSave.getGreeny1());
//        group.setGreeny2(lastSave.getGreeny2());
//        group.setGreenyPointsTeam1(lastSave.getGreenyPointsTeam1());
//        group.setGreenyPointsTeam2(lastSave.getGreenyPointsTeam2());
//        group.setGreeny1Details(lastSave.getGreeny1Details());
//        group.setGreeny2Details(lastSave.getGreeny2Details());
        System.out.println("Number of presses = " + currentNumberOfPresses + " " + Arrays.toString(group.getPressHoles()) + " " + Arrays.toString(group.getPressHolesTeam1()));

//        switch (buttonName) {
//            case "pressTeam1":
//
//                System.out.println("Team 1 pressing on the " + (nextHoleNumber) + "th hole");
//                group.getPresses()[currentNumberOfPresses] = nextHoleNumber;
//                group.getPressDescriptions()[currentNumberOfPresses] = "Team 1 pressed on " + nextHoleNumber;
//                currentNumberOfPresses += 1;
//                group.setNumberOfPresses(currentNumberOfPresses);
//                break;
//            case "pressTeam2":
//                System.out.println("Team 2 pressing on the " + (nextHoleNumber) + "th hole");
//                group.getPresses()[currentNumberOfPresses] = nextHoleNumber;
//                group.getPressDescriptions()[currentNumberOfPresses] = "Team 2 pressed on " + nextHoleNumber;
//                currentNumberOfPresses += 1;
//                group.setNumberOfPresses(currentNumberOfPresses);
//                break;
//            case "greenyTeam1":
//                int greeny1Total = 0;
//                for (int i = 0; i < 10; i++) {
//                    if (group.getGreenyPointsTeam1()[i] == 0) {
//                        group.getGreenyPointsTeam1()[i] = nextHoleNumber - 1;
//                        greeny1Total += getLBLTPointsForHole(group.getGreenyPointsTeam1()[i], group.getPresses());
//                        String g1Details = group.getGreeny1Details();
//                        g1Details += " " + (nextHoleNumber - 1);
//                        group.setGreeny1Details(g1Details);
//                        i = 10;
//                    } else {
//                        greeny1Total += getLBLTPointsForHole(group.getGreenyPointsTeam1()[i], group.getPresses());
//
//                    }
//                }
//                group.setGreeny1(greeny1Total);
//                break;
//            case "greenyTeam2":
//                int greeny2Total = 0;
//                for (int i = 0; i < 10; i++) {
//                    if (group.getGreenyPointsTeam2()[i] == 0) {
//                        group.getGreenyPointsTeam2()[i] = nextHoleNumber - 1;
//                        greeny2Total += getLBLTPointsForHole(group.getGreenyPointsTeam2()[i], group.getPresses());
//                        String g2Details = group.getGreeny2Details();
//                        g2Details += " " + (nextHoleNumber - 1);
//                        group.setGreeny2Details(g2Details);
//                        i = 10;
//                    } else {
//                        greeny2Total += getLBLTPointsForHole(group.getGreenyPointsTeam2()[i], group.getPresses());
//                    }
//                }
//                group.setGreeny2(greeny2Total);
//                break;
//
//        }
        int gameId = group.getGameId();
        System.out.println("In addGroupScoreSubmit gameId=" + gameId);
        int counter = 0; //There will always be four group players (for now)

        //***********************************************************************
        // Get the scorecard from the session.  This will have info about the 
        // current game such as pars.
        //***********************************************************************
        ScoreCard scorecard = (ScoreCard) request.getSession().getAttribute("scorecard");

        //****************************************************************************
        // For each player, go through and process 
        //****************************************************************************
        group.setPlayerIds(lastSave.getPlayerIds()); //These dont come from the form.
        System.out.println("Saving group: " + group);

        for (int currentPlayerId : lastSave.getPlayerIds()) {
            //int currentPlayerId = user.getUserId();
            System.out.print(currentPlayerId);

            //Add up the scores on the front
            for (int score : group.getPlayerScoresFront()[counter]) {
                System.out.print(score + " ");
                group.getPlayerScoreFront()[counter] += score;
            }
            System.out.println(group);

            //Add up the scores on the back
            for (int score : group.getPlayerScoresBack()[counter]) {
                System.out.print(score + " ");
                group.getPlayerScoreBack()[counter] += score;
            }
            System.out.println("");

            //Create an array for all 18 holes.
            int[] holesAll = new int[18];
            for (int i = 0; i < 9; i++) {
                holesAll[i] = group.getPlayerScoresFront()[counter][i];
                holesAll[i + 9] = group.getPlayerScoresBack()[counter][i];
            }
            
            //save their 18 hole score
            group.getAll18()[counter] = holesAll;
            
            //*******************************************************************
            //Calculate Chicago Points
            //*******************************************************************
            int chicagoPoints = UtilGames.getChicagoPoints(request, scorecard.getGameHoleParArrayList(), holesAll);
            group.getChicagoPoints()[counter] = chicagoPoints;

            //*******************************************************************
            //Persist the changed scores to the database.
            //*******************************************************************
            GamePlayer gp = null;
            try {
                gp = gpr.findByGameIdAndUserId(gameId, currentPlayerId).get(0);
            } catch (Exception e) {
                gp = new GamePlayer(0, gameId, currentPlayerId);
            }
            gp.setScore(group.getPlayerScoreFront()[counter] + group.getPlayerScoreBack()[counter]);
            gp.setPoints(chicagoPoints);
            gpr.save(gp);

            //*******************************************************************
            //Also have to save the scores if they were not changed from last time.
            //Create an array for all 18 holes.
            //*******************************************************************
            int[] holesAllLastTime = new int[18];
            try {
                for (int i = 0; i < 9; i++) {
                    holesAllLastTime[i] = lastSave.getPlayerScoresFront()[counter][i];
                    holesAllLastTime[i + 9] = lastSave.getPlayerScoresBack()[counter][i];
                }
            } catch (NullPointerException npe) {
                Arrays.fill(holesAllLastTime, 0); //If the first time there would not be a last time.
            }

            //*******************************************************************
            //Check to see what's changed.
            //*******************************************************************
            for (int i = 0; i < holesAll.length; i++) {
                if (holesAll[i] != holesAllLastTime[i]) {
                    Score score;
                    try {
                        score = sr.findByGameIdAndUserIdAndHoleNumber(gameId, currentPlayerId, i + 1).get(0);
                    } catch (Exception e) {
                        score = new Score(0, gameId, currentPlayerId);
                    }
                    score.setHoleNumber(i + 1);
                    score.setShotsTaken(holesAll[i]); //Set their shots to the new value.

                    score.setPar(Integer.parseInt(scorecard.getGameHoleParArrayList().get(i)));
                    score.setPoints(UtilGames.getChicagoPoints(request, score.getPar(), score.getShotsTaken()));

                    //BJM Todo Is there a reason to store the points/par for the score?  This can always be 
                    //derived later.
                    System.out.println("Saving score to db");
                    sr.save(score);
                }
            }
            //Increment counter for next player
            counter++;
        }

        boolean playingLBLT = true;
        if (playingLBLT) {

            System.out.println("*************** LBLT Details *******************");

            //************************************************************************
            // Low Ball Low Total Processing
            //**************************************
            //Have to go through the scores with p1/p2 compared with p3/p4.  The totals
            //are added and compared to each other to give point for total.  The lowest
            //score for the group is also checked for another point for low ball.  Have to 
            //watch out for carries as well.
            //************************************************************************
            //******************************************************************
            // This data structure will be used to hold the lblt scores.  Each element 
            // will be used to hold the points on the holes that the teams get.  Row 0 will
            // hold the low for team 1, row1 will hold totals for team 1.  Row 2 will 
            // hold low for team 2, and row 3 will hold totals for team 2.EPLS...
            //******************************************************************
            //int[][] lblt = lastSave.getLblt();
            int[][] lblt = new int[4][18];
            int[] lbltTotals = new int[4];

            //******************************************************************
            //Go over the holes and determine who is the winners on the hole.  
            //******************************************************************
            final int PLAYER1 = 0;
            final int PLAYER2 = 1;
            final int PLAYER3 = 2;
            final int PLAYER4 = 3;

            final int TEAM1_LOW = 0;
            final int TEAM2_LOW = 2;
            final int TEAM1_TOTAL = 1;
            final int TEAM2_TOTAL = 3;

            int lastLowWin = 0; //Note these hold the actual hole number (not index)
            int lastTotalWin = 0;

            for (int currentHoleNumber = 0; currentHoleNumber < nextHoleNumber - 1; currentHoleNumber++) {

                int team1Total = scores[PLAYER1][currentHoleNumber] + scores[PLAYER2][currentHoleNumber];
                int team2Total = scores[PLAYER3][currentHoleNumber] + scores[PLAYER4][currentHoleNumber];

                System.out.println("Totals:  team1=" + team1Total + " team2=" + team2Total);

                int team1Low = scores[PLAYER1][currentHoleNumber];
                if (scores[PLAYER2][currentHoleNumber] < team1Low) {
                    team1Low = scores[PLAYER2][currentHoleNumber];
                }

                int team2Low = scores[PLAYER3][currentHoleNumber];
                if (scores[PLAYER4][currentHoleNumber] < team2Low) {
                    team2Low = scores[PLAYER4][currentHoleNumber];
                }

                System.out.println("Lows:  team1=" + team1Low + " team2=" + team2Low);

                group.getCarryingLowTotal()[0] = 0;
                if (team1Low < team2Low) {
                    System.out.println("team 1 wins low");
                    //******************************************************************
                    //Next want to go down the arrays and fill in the carries.
                    //******************************************************************
                    for (int i = currentHoleNumber; i >= lastLowWin; i--) {
                        System.out.println("setting low for team 1 from " + currentHoleNumber + " to " + lastLowWin);
                        int points = getLBLTPointsForHole(i + 1, group.getPressHoles());
                        lblt[TEAM1_LOW][i] = points;
                        lbltTotals[TEAM1_LOW] += points;
                    }
                    lastLowWin = currentHoleNumber + 1;
                } else if (team1Low > team2Low) {
                    System.out.println("Team 2 wins low for hole");
                    //******************************************************************
                    //Next want to go down the arrays and fill in the carries.
                    //******************************************************************
                    for (int i = currentHoleNumber; i >= lastLowWin; i--) {
                        System.out.println("setting Low for team 2 from " + currentHoleNumber + " to " + lastLowWin);
                        int points = getLBLTPointsForHole(i + 1, group.getPressHoles());
                        lblt[TEAM2_LOW][i] = points;
                        lbltTotals[TEAM2_LOW] += points;
                    }
                    lastLowWin = currentHoleNumber + 1;
                } else {
                    //Carried low.  Update the carring stat
                    group.getCarryingLowTotal()[0] = (currentHoleNumber + 1 - lastLowWin);
                    System.out.println("Setting lows carried = " + currentHoleNumber + "-" + lastLowWin + "=" + (currentHoleNumber - lastLowWin));
                }

                group.getCarryingLowTotal()[1] = 0;
                if (team1Total < team2Total) {
                    System.out.println("Team 1 wins total");
                    //******************************************************************
                    //Next want to go down the arrays and fill in the carries.
                    //******************************************************************
                    for (int i = currentHoleNumber; i >= lastTotalWin; i--) {
                        System.out.println("setting total for team 1 from " + currentHoleNumber + " to " + lastTotalWin);
                        int points = getLBLTPointsForHole(i + 1, group.getPressHoles());
                        lblt[TEAM1_TOTAL][i] = points;
                        lbltTotals[TEAM1_TOTAL] += points;
                    }
                    lastTotalWin = currentHoleNumber + 1;
                } else if (team1Total > team2Total) {
                    System.out.println("Team 2 wins total");
                    //******************************************************************
                    //Next want to go down the arrays and fill in the carries.
                    //******************************************************************
                    for (int i = currentHoleNumber; i >= lastTotalWin; i--) {
                        System.out.println("setting total for team 2 from " + currentHoleNumber + " to " + lastTotalWin);
                        int points = getLBLTPointsForHole(i + 1, group.getPressHoles());
                        lblt[TEAM2_TOTAL][i] = points;
                        lbltTotals[TEAM2_TOTAL] += points;
                    }
                    lastTotalWin = currentHoleNumber + 1;
                } else {
                    //Carried low.  Update the carring stat
                    System.out.println("Setting totals carried = " + currentHoleNumber + "-" + lastTotalWin + "=" + (currentHoleNumber - lastTotalWin));
                    group.getCarryingLowTotal()[1] = (currentHoleNumber + 1 - lastTotalWin);
                }
            }

            //******************************************************
            //Show the output
            //******************************************************
            System.out.println("LBLT SCORES");
            for (int i = 0; i < 4; i++) {
                System.out.println(Arrays.toString(lblt[i]));
                System.out.println("Total=" + lbltTotals[i]);
            }

            group.setLblt(lblt);
            group.setLbltTotals(lbltTotals);

            //******************************************************************
            //Derive the birdy points based on the score
            //******************************************************************
            
            String[] temp= UtilGames.findBirdies(1, group.getAll18(), scorecard.getGameHoleParArrayList());
            group.setBirdyHolesTeam1(temp);
            temp= UtilGames.findBirdies(2, group.getAll18(), scorecard.getGameHoleParArrayList());
            group.setBirdyHolesTeam2(temp);
            
            //*************************************************************
            //Persist to the database
            //*************************************************************
            GameLBLT gameLBLT = new GameLBLT(gameId);
            boolean firstTeam1 = true;
            boolean firstTeam2 = true;
            String pressDetailsTeam1 = "";
            String pressDetailsTeam2 = "";
            int counterPresses = 0;

            int team1Total = lbltTotals[0] + lbltTotals[1];
            int team2Total = lbltTotals[2] + lbltTotals[3];
            //Add the points for the Greenies
            for (String current : group.getGreenyHolesTeam1()) {
                try {
                    team1Total += getLBLTPointsForHole(Integer.parseInt(current), group.getPressHoles());
                } catch (Exception e) {
                    System.out.println(" could not add");
                }
            }
            for (String current : group.getGreenyHolesTeam2()) {
                try {
                    team2Total += getLBLTPointsForHole(Integer.parseInt(current), group.getPressHoles());
                } catch (Exception e) {
                    System.out.println(" could not add");
                }
            }

            //Add the points for the birdies
            for (String current : group.getBirdyHolesTeam1()) {
                try {
                    team1Total += getLBLTPointsForHole(Integer.parseInt(current), group.getPressHoles());
                } catch (Exception e) {
                    System.out.println(" could not add");
                }
            }
            for (String current : group.getBirdyHolesTeam2()) {
                try {
                    team2Total += getLBLTPointsForHole(Integer.parseInt(current), group.getPressHoles());
                } catch (Exception e) {
                    System.out.println(" could not add");
                }
            }
            group.getTotalScore()[0] = team1Total;
            group.getTotalScore()[1] = team2Total;

            //***********************************************************************************
            //For each press, add to the right press string which will be going to the database.
            //***********************************************************************************

            gameLBLT.setTeam1presses(Utility.getCSV(group.getPressHolesTeam1()));
            gameLBLT.setTeam2presses(Utility.getCSV(group.getPressHolesTeam2()));

            gameLBLT.setTeam1GreenyPoints(Utility.getCSV(group.getGreenyHolesTeam1()));
            gameLBLT.setTeam2GreenyPoints(Utility.getCSV(group.getGreenyHolesTeam2()));

            gameLBLT.setTeam1BirdyPoints(Utility.getCSV(group.getBirdyHolesTeam1()));
            gameLBLT.setTeam2BirdyPoints(Utility.getCSV(group.getBirdyHolesTeam2()));

            glbltr.save(gameLBLT);
        } //playing lblt

        group.setCurrentPointValue(getLBLTPointsForHole(nextHoleNumber, group.getPressHoles()));

        //************************************************************************
        //Storing the group in session to compare next submit.  Will only have to 
        //save scores that were modified.
        //************************************************************************
        request.getSession().setAttribute("lastSave", group);
        model.addAttribute("group", group);

        return "/group/scoreLBLT";
    }

    /**
     * This method will double the points for each press which has been used
     * prior to the hole number passed in.
     *
     * @since 20170516
     * @author BJM
     *
     * @param holeNumber
     * @return
     */
    public int getLBLTPointsForHole(int holeNumber, int[] presses) {
        int points = 1;
        for (int currentPress : presses) {
            if (currentPress == 0) {
                break;//when we get to a 0 the rest should be 0 too.
            }
            if (currentPress <= holeNumber) {
                points *= 2;
            }
        }
        return points;
    }
}
