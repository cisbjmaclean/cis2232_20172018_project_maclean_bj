package info.hccis.admin.web.services;

import com.google.gson.Gson;
import info.hccis.admin.service.CodeService;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

//issues here...
//http://www.scriptscoop.net/t/ba4553fde8ca/spring-autowired-classes-are-null-in-jersey-rest.html
@Component
@Path("/codes")
@Scope("request")
public class RestWebServiceWithJPA {

    @Resource
    private final CodeService cs;

    public RestWebServiceWithJPA(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cs = applicationContext.getBean(CodeService.class);
    }

    @GET
    public Response getMsg(@PathParam("param") String theId) {

        String output = "Jersey say : " + theId;
        System.out.println("in Jersey hello web service");

        //output = cs.getCodeTypes().toString();// ctr.findOne(Integer.parseInt(theId)).getEnglishDescription();
        //Convert the output to json.
        Gson gson = new Gson();
        output = gson.toJson(cs.getCodeTypes());
        

        return Response.status(200).entity(output).build();

    }

}
