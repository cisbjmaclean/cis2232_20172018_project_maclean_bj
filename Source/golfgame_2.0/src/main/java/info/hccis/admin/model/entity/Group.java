package info.hccis.admin.model.entity;

import info.hccis.admin.model.jpa.User;
import java.util.ArrayList;
import java.util.Arrays;

public class Group {

    private ArrayList<Integer> playerIds = new ArrayList();
    private ArrayList<Integer> gamePlayerIds = new ArrayList();
    private ArrayList<User> users = new ArrayList();
    private int gameId = 0;
    private int[][] playerScoresFront = new int[4][9];
    private int[][] playerScoresBack = new int[4][9];
    private int[] playerScoreFront = new int[4];
    private int[] playerScoreBack = new int[4];
    private int[] chicagoPoints = new int[4];
    
    private int[][] all18 = new int[4][18]; //Used temp to hold all 18


    //LBLT attributes
    private int[][] lblt = new int[4][18];
    private int[] lbltTotals = new int[4];
    private int[] carryingLowTotal = new int[2];
//    private int[] presses = new int[10];
    private int numberOfPresses;
//    private String[] pressDescriptions = new String[10];
    private int currentPointValue = 1;
//    private int birdyPointsTeam1 = 0;
//    private int birdyPointsTeam2 = 0;
//    private int[] greenyPointsTeam1 = new int[10]; 
//    private int[] greenyPointsTeam2 = new int[10]; 
//    private int greeny1,greeny2;
//    private String greeny1Details = "Team 1 Greenies:";
//    private String greeny2Details = "Team 2 Greenies:";

    private String[] pressHolesTeam1 = new String[18];
    private String[] pressHolesTeam2 = new String[18];
    private int[] pressHoles = new int[18];
    private String[] greenyHolesTeam1 = new String[18];
    private String[] greenyHolesTeam2 = new String[18];
    private String[] birdyHolesTeam1 = new String[18];
    private String[] birdyHolesTeam2 = new String[18];

    private String[] lowTotal = {"Low", "Total"};

    private int[] totalScore = new int[2];
    private int loaded = 1;
    
    public Group() {
        for (int i = 0; i < 4; i++) {
            playerIds.add(0);
            gamePlayerIds.add(0);
        }

        for (int i = 0; i < 18; i++) {
            pressHolesTeam1[i] = "";
            pressHolesTeam2[i] = "";
            greenyHolesTeam1[i] = "";
            greenyHolesTeam2[i] = "";
            birdyHolesTeam1[i] = "";
            birdyHolesTeam2[i] = "";
        }

    }

    public ArrayList<Integer> getPlayerIds() {
        return playerIds;
    }

    public void setPlayerIds(ArrayList<Integer> playerIds) {
        this.playerIds = playerIds;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int[][] getPlayerScoresFront() {
        return playerScoresFront;
    }

    public void setPlayerScoresFront(int[][] playerScoresFront) {
        this.playerScoresFront = playerScoresFront;
    }

    public int[][] getPlayerScoresBack() {
        return playerScoresBack;
    }

    public void setPlayerScoresBack(int[][] playerScoresBack) {
        this.playerScoresBack = playerScoresBack;
    }

    public int[] getPlayerScoreFront() {
        return playerScoreFront;
    }

    public void setPlayerScoreFront(int[] playerScoreFront) {
        this.playerScoreFront = playerScoreFront;
    }

    public int[] getPlayerScoreBack() {
        return playerScoreBack;
    }

    public void setPlayerScoreBack(int[] playerScoreBack) {
        this.playerScoreBack = playerScoreBack;
    }

    public int[] getChicagoPoints() {
        return chicagoPoints;
    }

    public void setChicagoPoints(int[] chicagoPoints) {
        this.chicagoPoints = chicagoPoints;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public ArrayList<Integer> getGamePlayerIds() {
        return gamePlayerIds;
    }

    public void setGamePlayerIds(ArrayList<Integer> gamePlayerIds) {
        this.gamePlayerIds = gamePlayerIds;
    }

    public int[][] getLblt() {
        return lblt;
    }

    public void setLblt(int[][] lblt) {
        this.lblt = lblt;
    }

    public int[] getLbltTotals() {
        return lbltTotals;
    }

    public void setLbltTotals(int[] lbltTotals) {
        this.lbltTotals = lbltTotals;
    }

    public int[] getCarryingLowTotal() {
        return carryingLowTotal;
    }

    public void setCarryingLowTotal(int[] carryingLowTotal) {
        this.carryingLowTotal = carryingLowTotal;
    }
//
//    public int[] getPresses() {
//        return presses;
//    }
//
//    public void setPresses(int[] presses) {
//        this.presses = presses;
//    }

    public int getNumberOfPresses() {
        return numberOfPresses;
    }

    public void setNumberOfPresses(int numberOfPresses) {
        this.numberOfPresses = numberOfPresses;
    }
//
//    public String[] getPressDescriptions() {
//        return pressDescriptions;
//    }
//
//    public void setPressDescriptions(String[] pressDescriptions) {
//        this.pressDescriptions = pressDescriptions;
//    }

    public String[] getLowTotal() {
        return lowTotal;
    }

    public void setLowTotal(String[] lowTotal) {
        this.lowTotal = lowTotal;
    }

    public int getCurrentPointValue() {
        return currentPointValue;
    }

    public void setCurrentPointValue(int currentPointValue) {
        this.currentPointValue = currentPointValue;
    }
//
//    public int getBirdyPointsTeam1() {
//        return birdyPointsTeam1;
//    }
//
//    public void setBirdyPointsTeam1(int birdyPointsTeam1) {
//        this.birdyPointsTeam1 = birdyPointsTeam1;
//    }
//
//
//
//    public int getBirdyPointsTeam2() {
//        return birdyPointsTeam2;
//    }
//
//    public void setBirdyPointsTeam2(int birdyPointsTeam2) {
//        this.birdyPointsTeam2 = birdyPointsTeam2;
//    }
//
//    public int[] getGreenyPointsTeam1() {
//        return greenyPointsTeam1;
//    }
//
//    public void setGreenyPointsTeam1(int[] greenyPointsTeam1) {
//        this.greenyPointsTeam1 = greenyPointsTeam1;
//    }
//
//    public int[] getGreenyPointsTeam2() {
//        return greenyPointsTeam2;
//    }
//
//    public void setGreenyPointsTeam2(int[] greenyPointsTeam2) {
//        this.greenyPointsTeam2 = greenyPointsTeam2;
//    }
//
//    public int getGreeny1() {
//        return greeny1;
//    }
//
//    public void setGreeny1(int greeny1) {
//        this.greeny1 = greeny1;
//    }
//
//    public int getGreeny2() {
//        return greeny2;
//    }
//
//    public void setGreeny2(int greeny2) {
//        this.greeny2 = greeny2;
//    }
//
//    public String getGreeny1Details() {
//        return greeny1Details;
//    }
//
//    public void setGreeny1Details(String greeny1Details) {
//        this.greeny1Details = greeny1Details;
//    }
//
//    public String getGreeny2Details() {
//        return greeny2Details;
//    }
//
//    public void setGreeny2Details(String greeny2Details) {
//        this.greeny2Details = greeny2Details;
//    }

    public String[] getPressHolesTeam1() {
        return pressHolesTeam1;
    }



    public void setPressHolesTeam1(String[] pressHolesTeam1) {
        this.pressHolesTeam1 = pressHolesTeam1;
    }

    public String[] getPressHolesTeam2() {
        return pressHolesTeam2;
    }


    public void setPressHolesTeam2(String[] pressHolesTeam2) {
        this.pressHolesTeam2 = pressHolesTeam2;
    }

    public String[] getGreenyHolesTeam1() {
        return greenyHolesTeam1;
    }

    public void setGreenyHolesTeam1(String[] greenyHolesTeam1) {
        this.greenyHolesTeam1 = greenyHolesTeam1;
    }

    public String[] getGreenyHolesTeam2() {
        return greenyHolesTeam2;
    }

    public void setGreenyHolesTeam2(String[] greenyHolesTeam2) {
        this.greenyHolesTeam2 = greenyHolesTeam2;
    }

    public String[] getBirdyHolesTeam1() {
        return birdyHolesTeam1;
    }

    public void setBirdyHolesTeam1(String[] birdyHolesTeam1) {
        this.birdyHolesTeam1 = birdyHolesTeam1;
    }

    public String[] getBirdyHolesTeam2() {
        return birdyHolesTeam2;
    }

    public void setBirdyHolesTeam2(String[] birdyHolesTeam2) {
        this.birdyHolesTeam2 = birdyHolesTeam2;
    }

    public int[] getPressHoles() {
        return pressHoles;
    }

    public void setPressHoles(int[] pressHoles) {
        this.pressHoles = pressHoles;
    }

    public int[] getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int[] totalScore) {
        this.totalScore = totalScore;
    }

    public int getLoaded() {
        return loaded;
    }

    public void setLoaded(int loaded) {
        this.loaded = loaded;
    }

    public int[][] getAll18() {
        return all18;
    }

    public void setAll18(int[][] all18) {
        this.all18 = all18;
    }


    
    
    public String toString() {
        String output = "\nGroup object\n"
                + "playerIds:"
                + playerIds + "\n"
                + Arrays.toString(playerScoresFront);
        return output;
    }

}
