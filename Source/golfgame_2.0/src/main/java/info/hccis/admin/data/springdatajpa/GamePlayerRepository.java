package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.GamePlayer;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GamePlayerRepository extends CrudRepository<GamePlayer, Integer> {

    List<GamePlayer> findByGameId(int gameId);
    List<GamePlayer> findByGameIdAndUserId(int gameId, int userId);

}