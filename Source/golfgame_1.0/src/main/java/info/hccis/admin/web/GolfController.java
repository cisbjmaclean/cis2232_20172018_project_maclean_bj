package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.GameDetailsRepository;
import info.hccis.admin.data.springdatajpa.UserAccessRepository;
import info.hccis.admin.model.jpa.GameDetails;
import info.hccis.admin.model.entity.UserAccess;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GolfController {

    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;
    private final GameDetailsRepository gameDetailsRepository;
    private final UserAccessRepository userAccessRepository;

    @Autowired
    public GolfController(UserAccessRepository userAccessRepository, GameDetailsRepository gameDetailsRepository, CodeTypeRepository ctr, CodeValueRepository cvr) {
        this.ctr = ctr;
        this.cvr = cvr;
        this.gameDetailsRepository = gameDetailsRepository;
        this.userAccessRepository = userAccessRepository;
    }

    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") UserAccess user, HttpServletRequest request) {

        /*
        
        See the @ModelAttribute refers to the object that is used on the form.  This is
        obtained here in the controller.  This is a common way that the controller gets access
        to the object and its attributes.  
        
         */
//Test to show what was passed in.  
        System.out.println("BJM in /golf/authenticate, username passed in to controller is:" + user.getUsername());

        /*
        
        Here we will store the user in the session.  It will then be available to 
        other future requests.  
        
         */
        request.getSession().setAttribute("user", user);

        /*
        
        Set an object in the model to be used on the next form.  
        
         */
        GameDetails gameDetails = new GameDetails();
        gameDetails.setTeam1Name("test");
        model.addAttribute("gameDetails", gameDetails);

        /*
        
        Specify the return.  This is the next view that will be presented to the user.
         */
        return "golf/setup";
    }

    @RequestMapping("/golf/setup")
    public String setup(Model model) {

        GameDetails gameDetails = new GameDetails();
        gameDetails.setGameId(1);

        model.addAttribute("gameDetails", gameDetails);
        return "golf/setup";
    }

    @RequestMapping("/golf/addGameDetails")
    public String addGameDetails(Model model, @ModelAttribute("gameDetails") GameDetails gameDetails, HttpServletRequest request) {
        System.out.println("BJM game details = " + gameDetails);
        UserAccess user = (UserAccess) request.getSession().getAttribute("user");
        gameDetails.setUserAccessId(user);
        userAccessRepository.save(user);
        gameDetailsRepository.save(gameDetails);
        return "golf/addScore";
    }

    @RequestMapping("/golf/addScore")
    public String addScore(Model model) {
        return "golf/addScore";
    }

    @RequestMapping("/golf/scorecard")
    public String showScorecard(Model model) {
        return "golf/scorecard";
    }

    @RequestMapping("/")
    public String showHome(Model model) {
        UserAccess user = new UserAccess();
        model.addAttribute("user", user);
        return "golf/welcome";
    }

}
