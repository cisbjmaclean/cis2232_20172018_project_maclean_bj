package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.GameDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameDetailsRepository extends CrudRepository<GameDetails, Integer> {

}