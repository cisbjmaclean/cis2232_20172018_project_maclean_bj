/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.entity;

import info.hccis.admin.model.jpa.GameDetails;
import info.hccis.admin.model.jpa.GameDetails;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author bjmaclean
 */
@Entity
@Table(name = "useraccess")
public class UserAccess implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userAccessId")
    private List<GameDetails> gamedetailsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userAccessId")
    private List<GameDetails> gameDetailsList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userAccessId")
    private Integer userAccessId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userType")
    private int userType;
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;

    @Transient
    private String userTypeDescription;
    
    public UserAccess() {
    }

    public UserAccess(Integer userAccessId) {
        this.userAccessId = userAccessId;
    }

    public UserAccess(Integer userAccessId, String username, String password, int userType) {
        this.userAccessId = userAccessId;
        this.username = username;
        this.password = password;
        this.userType = userType;
    }

    public String getUserTypeDescription() {
        return userTypeDescription;
    }

    public void setUserTypeDescription(String userTypeDescription) {
        this.userTypeDescription = userTypeDescription;
    }

    
    
    public Integer getUserAccessId() {
        return userAccessId;
    }

    public void setUserAccessId(Integer userAccessId) {
        this.userAccessId = userAccessId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userAccessId != null ? userAccessId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccess)) {
            return false;
        }
        UserAccess other = (UserAccess) object;
        if ((this.userAccessId == null && other.userAccessId != null) || (this.userAccessId != null && !this.userAccessId.equals(other.userAccessId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.entity.Useraccess[ userAccessId=" + userAccessId + " ]";
    }

    public List<GameDetails> getGameDetailsList() {
        return gameDetailsList;
    }

    public void setGameDetailsList(List<GameDetails> gameDetailsList) {
        this.gameDetailsList = gameDetailsList;
    }

    public List<GameDetails> getGamedetailsList() {
        return gamedetailsList;
    }

    public void setGamedetailsList(List<GameDetails> gamedetailsList) {
        this.gamedetailsList = gamedetailsList;
    }
    
}
