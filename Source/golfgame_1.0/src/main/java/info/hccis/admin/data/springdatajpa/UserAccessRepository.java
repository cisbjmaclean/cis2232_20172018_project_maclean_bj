package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.entity.UserAccess;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccessRepository extends CrudRepository<UserAccess, Integer> {


}