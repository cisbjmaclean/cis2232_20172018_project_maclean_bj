/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import info.hccis.admin.model.entity.UserAccess;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author BJMacLean
 */
@Entity
@Table(name = "gamedetails")
public class GameDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gameId")
    private Integer gameId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "team1Name")
    private String team1Name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "team1Player1")
    private String team1Player1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "team1Player2")
    private String team1Player2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "team2Name")
    private String team2Name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "team2Player1")
    private String team2Player1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "team2Player2")
    private String team2Player2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amountPerPoint")
    private long amountPerPoint;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pressesPerTeam")
    private int pressesPerTeam;
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
    @Size(max = 20)
    @Column(name = "createdUserId")
    private String createdUserId;
    @Column(name = "updatedDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDateTime;
    @Size(max = 20)
    @Column(name = "updatedUserId")
    private String updatedUserId;
    @JoinColumn(name = "userAccessId", referencedColumnName = "userAccessId")
    @ManyToOne(optional = false)
    private UserAccess userAccessId;

    public GameDetails() {
    }

    public GameDetails(Integer gameId) {
        this.gameId = gameId;
    }

    public GameDetails(Integer gameId, String team1Name, String team1Player1, String team1Player2, String team2Name, String team2Player1, String team2Player2, long amountPerPoint, int pressesPerTeam) {
        this.gameId = gameId;
        this.team1Name = team1Name;
        this.team1Player1 = team1Player1;
        this.team1Player2 = team1Player2;
        this.team2Name = team2Name;
        this.team2Player1 = team2Player1;
        this.team2Player2 = team2Player2;
        this.amountPerPoint = amountPerPoint;
        this.pressesPerTeam = pressesPerTeam;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam1Player1() {
        return team1Player1;
    }

    public void setTeam1Player1(String team1Player1) {
        this.team1Player1 = team1Player1;
    }

    public String getTeam1Player2() {
        return team1Player2;
    }

    public void setTeam1Player2(String team1Player2) {
        this.team1Player2 = team1Player2;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public String getTeam2Player1() {
        return team2Player1;
    }

    public void setTeam2Player1(String team2Player1) {
        this.team2Player1 = team2Player1;
    }

    public String getTeam2Player2() {
        return team2Player2;
    }

    public void setTeam2Player2(String team2Player2) {
        this.team2Player2 = team2Player2;
    }

    public long getAmountPerPoint() {
        return amountPerPoint;
    }

    public void setAmountPerPoint(long amountPerPoint) {
        this.amountPerPoint = amountPerPoint;
    }

    public int getPressesPerTeam() {
        return pressesPerTeam;
    }

    public void setPressesPerTeam(int pressesPerTeam) {
        this.pressesPerTeam = pressesPerTeam;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public UserAccess getUserAccessId() {
        return userAccessId;
    }

    public void setUserAccessId(UserAccess userAccessId) {
        this.userAccessId = userAccessId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gameId != null ? gameId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GameDetails)) {
            return false;
        }
        GameDetails other = (GameDetails) object;
        if ((this.gameId == null && other.gameId != null) || (this.gameId != null && !this.gameId.equals(other.gameId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.Gamedetails[ gameId=" + gameId + " ]";
    }
    
}
