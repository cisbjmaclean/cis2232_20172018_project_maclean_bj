use cis2232_golf_game;

--
-- Table structure for table `gamedetails`
--

CREATE TABLE `GameDetails` (
  `gameId` int(5)   NOT NULL AUTO_INCREMENT COMMENT 'FK to game table',
  `userAccessId` int(3) NOT NULL COMMENT 'FK to userAccess',
  `team1Name` varchar(30) NOT NULL,
  `team1Player1` varchar(30) NOT NULL,
  `team1Player2` varchar(30) NOT NULL,
  `team2Name` varchar(30) NOT NULL,
  `team2Player1` varchar(30) NOT NULL,
  `team2Player2` varchar(30) NOT NULL,
  `amountPerPoint` decimal(10,0) NOT NULL,
  `pressesPerTeam` int(2) NOT NULL,
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL,
   PRIMARY KEY ( gameId )
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gamescore`
--

CREATE TABLE `GameScore` (
  `gameId` int(11) NOT NULL,
  `holeNumber` int(11) NOT NULL,
  `t1p1Score` int(11) NOT NULL,
  `t1p2Score` int(11) NOT NULL,
  `t2p1Score` int(11) NOT NULL,
  `t2p2Score` int(11) NOT NULL,
  `lowWinner` int(11) NOT NULL,
  `totalWinner` int(11) NOT NULL,
  `teamPress` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for table `gamescore`
--
ALTER TABLE `GameScore`
  ADD PRIMARY KEY (`gameId`,`holeNumber`);

ALTER TABLE GameDetails
    ADD FOREIGN KEY (userAccessId)
    REFERENCES UserAccess(userAccessId);

ALTER TABLE GameScore
    ADD FOREIGN KEY (gameId)
    REFERENCES GameDetails(gameId);
