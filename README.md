# README #

Welcome to the cis2232 2017/2018 course example project.  This is a low ball/low total golf game web application.  It will contain elements from the CIS2232 course outcomes.  

### How do I get set up? ###

* Clone the repository and open in Netbeans
* Database configuration - run the src/main/resources/ databaseCreation scripts in the order specified.
* Run the application from netbeans using Tomcat web server.

### Contribution guidelines ###

* Developer:  BJ MacLean
* Peer Reviewer:  TBD

### Who do I talk to? ###

BJ MacLean